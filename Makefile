N = book

all: $N.pdf $N-book-book.pdf

%.pdf: %.tex
	pdflatex $^
	pdflatex $^
	pdflatex $^

%-book-book.pdf: %.pdf
	pdfbook --papersize '{8.5in,5.5in}' --no-landscape $*.pdf
	pdfbook --papersize '{11in,8.5in}' $*-book.pdf

clean: mostlyclean
	rm -f $N.pdf

mostlyclean:
	rm -f $N.aux $N.log $N.out $N.toc

# food for GNU make
.PHONY: all clean mostlyclean
